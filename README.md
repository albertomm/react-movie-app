# 🎥 TMDB Movie App

Discover the most popular, top rated and latest movies. Users can also find a movie based on the movie title.

![Mobile screenshot](https://i.ibb.co/X5mbkYd/TMDBPhone.webp)
![Homepage screenshot](https://i.ibb.co/G3LLt1K/TMDBWeb2.jpg)

## Tools

- [React](https://reactjs.org/)
- [Axios](https://www.npmjs.com/package/axios)
- [TMDB Rest API](https://www.themoviedb.org/documentation/api?language=en-US)

## Installation

React Setup 

    npx create-react-app .

Fetch Date

    npm i axios
    
Runs the app on localhost:3000
    
    npm start
    
Builds the app for production to the build folder.

     npm run build
    
    
## Author

  Alberto Moura - [github.com/MMnemonic](https://github.com/MMnemonic)

  Project Link: [https://festive-wilson-a51c32.netlify.app/](https://festive-wilson-a51c32.netlify.app/)
  
  
## License
  
  Released under the MIT License.
